#!/usr/bin/python3.6
from bs4 import BeautifulSoup as bs
import requests

from item import Item

def search(query):
	url = 'https://www.bdodae.com/data/search.php?q={}'.format(query.replace(' ', '%20'))
	data = requests.get(url)
	soup = bs(data.text, features='lxml')
	results = soup.find_all('a')
	return results

def parse(item):
	url = 'https://www.bdodae.com' + item
	data = requests.get(url)
	soup = bs(data.text, features='lxml')
	r = soup.find("div", {"id": "item_recipe"})
	items = list()
	
	divs = r.find_all("div", {"class": "ing1_box"})
	for ing in divs:
		l = find_items(ing)
		items.append(l)
	return items

def find_items(ing):
	ingdata = ing.find_all("div", {"class": "ing_item"})
	d = 1
	l = None
	last = list()
	last_item = None
	last_ing = None
	for x in ingdata:
		d = int(str(x)[15])
		item = to_item(x)
		
		if len(last) < d:
			last.append(item)
		
		if d == 1:
			l = item
			last[d-1] = item
			
		elif d > 1:
			last[d-2].add_ing(item)
			last[d-1] = item
			
	return(l)
	
def to_item(str1):
	a = str(str1.find("a"))
	a_end = a[a.find('>')+1:-4]
	name = a_end[:a_end.find('(')-1]
	i = (str(str1).find(a)-4)
	x = str(str1)[i:i+4]
	x = x[x.find('>')+1:]
	amount = int(x)
	
	item = Item(name, amount, None)
	return item
