#!/usr/bin/python3.6
import item

class Recipe:
	id = 0
	inglist = None
	name = ''

	def __init__(self, id, name, ing):
		self.id = id
		self.inglist = ing
		self.name = name
		
	def print(self):
		print(self.id, self.name, self.inglist)
		
	def pretty(self):
		print('{} ({})'.format(self.name, self.id))
		for ing in self.inglist:
			print('\t{} x{}'.format(ing[0], ing[1]))