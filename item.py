#!/usr/bin/python3.6
import sys

class Item:
	name = ''
	amount = 0
	inglist = None

	def __init__(self, name, amount, ing):
		self.name = name
		self.amount = amount
		self.inglist = ing
	
	def text(self):
		return('{}, {}, {}'.format(self.name,self.amount,self.inglist))
		
	def add_ing(self, ing):
		if self.inglist is None:
			self.inglist = list()
		self.inglist.append(ing)
		
def print_recipe(item, depth=0):
	print('{} x{}'.format(item.name, item.amount))
	if item.inglist is not None:
		for x in item.inglist:
			for _ in range(depth+1):
				sys.stdout.write('\t')
			print_recipe(x, depth+1)
		