#!/usr/bin/python3.6
import get_items
import string
import db_builder as db

ids = None
recipes = None



def test_items(items):
	items = db.parse_data(items)
	old_items = db.load('db/items.db')
	
	for i in items:
		if i not in old_items:
			raise ValueError('db/Missing item', i, ids.get(i))
	
def test_nodes(nodes):
	nodes = db.parse_data(nodes)
	old_nodes = db.load('db/nodes.db')
	
	for n in nodes:
		if n not in old_nodes:
			raise ValueError('Missing node', n)
	
def test_recipes():
	print('Testing recipes...')
	for i in recipes:
		print('\t', i)
		r = recipes.get(i)
		print('\t', r)

		if r:
			print('\t', r.name)
			for ing in r.inglist:
				print('\t\t', ids.get(ing[0]), ing[1])
		else:
			raise ValueError('Invalid recipe', i, r)
			
def test_lookup():
	n = input('Search for recipe: ').replace(' ', '_').lower()
	n = 'r-{}'.format(n)
	print(n)
	id = db.calc_id(n)
	#print(id)
	r = recipes.get(id)
	r.pretty()

def test():
	print('Getting data...')
	items, nodes = get_items.get_all(string.ascii_lowercase)
	print()
	print('Started testing...')
	test_items(items)
	print('All items included')
	test_nodes(nodes)
	print('All nodes included')
	test_recipes()
	print('All recipes valid')
	print('Done.')
	return
	
if __name__ == '__main__':
	
	ids = db.load('db/ids.db')
	recipes = db.load('db/recipes.db')
	test()
	#test_lookup()
	#test_recipes()