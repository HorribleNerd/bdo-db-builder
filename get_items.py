#!/usr/bin/python3.6
import sys
import webparser

def get_all(alphabet):
	items = list()
	nodes = list()
	i = 0
	for c in alphabet:
		r = webparser.search(c)
		for a in r:
			s = str(a)
			if 'item_popup' in s:
				href = a.get('href')
				if href not in items:
					items.append(href)
			elif 'node_popup' in s:
				href = a.get('href')
				if href not in nodes:
					nodes.append(href)
		
		i += 1
		sys.stdout.write('\r[{}{}]'.format('='*i, ' '*(len(alphabet)-i)))
			
	return items, nodes