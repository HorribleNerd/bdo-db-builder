#!/usr/bin/python3.6
import pickle
import string
import get_items
import item
import webparser
import hashlib
import recipe
import test
import os

ids = dict()
id_length = 8

def store(data, name):
	if not os.path.exists('db/'):
		os.makedirs('db/')
	fw = open(name, 'wb')
	pickle.dump(data, fw)
	fw.close()
	
def load(file):
	fd = open(file, 'rb')
	dataset = pickle.load(fd)
	return dataset
	
def calc_id(name):
	global ids
	if name in ids.values():
		for k, v in ids.items():
			if v == name:
				return k
		
	m = hashlib.md5()	
	m.update(name.encode('utf-8'))
	id = str(m.hexdigest())[0:id_length]
	if id in ids.keys():
		for k, v in ids.items():
			if k == id and v != name:
				raise ValueError('ID Collision', id, name)
	ids[id] = name
	store(ids, 'db/ids.db')
	return id
	
def parse_data(data):
	pdata = dict()
	for i in data:
		name = i[i.find('=')+1:]
		id = calc_id(name)
		pdata[id] = [name, i]
	return pdata
	
def main():
	global ids
	print('Getting data...')
	try:
		ids = load('db/ids.db')
		recipes = load('db/recipes.db')
		items = load('db/items.db')
		nodes = load('db/nodes.db')
	except:
		ids = dict()
		items, nodes = get_items.get_all(string.ascii_lowercase)
		items = parse_data(items)
		nodes = parse_data(nodes)		
		store(items, 'db/items.db')
		store(nodes, 'db/nodes.db')
		recipes = dict()
	
	print()
	print('Parsing recipes')
	for i in items:
		#print(i)
		try:
			name = 'r-{}'.format(items.get(i)[0])
			id = calc_id(name)
			if id not in recipes.keys():
				ings = webparser.parse(items.get(i)[1])
				if ings:
					
					recipe_ings = list()
					for x in ings:
						ing_id = calc_id(x.name)
						recipe_ings.append((ing_id, x.amount))
					r = recipe.Recipe(id, name, recipe_ings)
					print(name)
					recipes[id] = r
					store(recipes, 'db/recipes.db')
					#r.print()
				
		except ValueError as e:
			print(e)
			return
		except Exception as e:
			#print(e)
			pass
	store(recipes, 'db/recipes.db')
	store(ids, 'db/ids.db')
	
	print('Stored all recipes')
	test.test(recipes)
	return
	
if __name__ == '__main__':
	main()
